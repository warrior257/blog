$(function () {
    var APPLICATION_ID = "843ECC71-0CA6-E00F-FF29-DCDEA91FB500",
       SECRET_KEY = "5B74CE01-28E1-A567-FFD9-19EE3CB11600",
       VERSION = "v1";
       
     Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
        
     var postsCollection = Backendless.Persistence.of(Posts).find();
     
     console.log(postsCollection);
     
     var wrapper = {
         post:postsCollection.data
     };
     Handlebars.registerHelper{'format',function (time) (
      return moment(time).format("dddd, mmmm bo yyyy"); 
   )};
    
    var blogscript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogscript);
    var blogHTML = blogTemplate(wrapper);

    $('.main-container').html(blogHTML);
    
});   
     
     function  Posts(args){ 
     args = args || {};
     this.title = args.title  || "";
     this.content = args.content || "";
     this.authorEmail = args.authorEmail ||"";
     }